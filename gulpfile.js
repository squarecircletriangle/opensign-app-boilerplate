var
	gulp   = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify');


gulp.task('copy-js-libs', function(){

	gulp.src([
			'bower_components/jquery/jquery.min.js',
			'bower_components/underscore/underscore-min.js',
			'bower_components/backbone/backbone-min.js',
			'bower_components/marionette/lib/backbone.marionette.min.js'
		])
		.pipe( concat('libs.js') )
		.pipe( uglify() )
		.pipe( gulp.dest('js/build') );

});


gulp.task('default', function(){

	gulp.run('copy-js-libs');

});