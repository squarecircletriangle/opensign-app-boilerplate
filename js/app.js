define([
	'marionette'
], function( Marionette ){

	var App = new Marionette.Application();

	App.addRegions({
		mainRegion     : '#container'
	});



	// INIIT
	App.on( 'initialize:after', function(){

		console.log('App has started!');

		if( Backbone.history ){
			Backbone.history.start();
		}

		App.trigger('home:show');

	});


	return App;
});


