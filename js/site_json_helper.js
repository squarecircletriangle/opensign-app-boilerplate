var SiteJSON = (function(){

	var _self      = {},
		siteJson   = {},
		loading    = false,
		jsonLoaded = false;



	// Load site.json via ajax and pass it to the callback proviced
	function load( callback ){

		// Call this only once
		if( loading || jsonLoaded ){ return; }

		$.ajax({
			url      : 'site.json',
			dataType : 'json'
		})
		.done(function(json){

			loading    = false;
			jsonLoaded = true;
			siteJson   = json;

			if( typeof callback === 'function' ){
				callback.call( window, siteJson );
			}
		})
		.fail(function(){

			loading = false;
			throw new Error('Site.json file could not be loaded.');
		});
	}



	// Get a navigation set by either its ID or name
	function getNavigationSet( nameOrId ){
		var allNavSets = siteJson.navigation_sets,
			searchObj,
			navSet;

		if( typeof nameOrId === 'string' && nameOrId.toLowerCase() === 'loose pages' ){
			// Return mock nav set object with loose pages as children
			return {
				id       : null,
				name     : 'Loose Pages',
				children : siteJson.loose_pages
			};
		}


		// Set search parameter depending on type of arguement passed
		if( typeof nameOrId === 'string' ){
			searchObj = {
				name : nameOrId
			};
		}
		else if( typeof nameOrId === 'number' && _isInt(nameOrId) ){
			searchObj = {
				id : nameOrId
			};
		}
		else {
			throw new Error( 'getNavigationSet: expecting id (integer) or name (string) as argument.' );
		}


		navSet = _.findWhere( allNavSets, searchObj );

		if( !navSet ){
			throw new Error( 'getNavigationSet: no navigation set with the name ' + name + ' could be found.' );
		}

		return navSet;
	}



	// Return all pages (of a certain template type) from a nav set
	function getAllPagesInNavigationSet( nameOrId, templateName ){
		
		var pages  = [];

		traverseNavSet( nameOrId, function( item, level ){

			// If templateName is given, only return those pages
			if( typeof templateName === 'string' ){
				if( item.template === templateName ){
					pages.push( item );
				}
			}
			else {
				pages.push( item );
			}
		});

		return pages;
	}


	// Traverse through a nav set an apply a function to each element
	function traverseNavSet( nameOrId, iteratorFn ){
		var navSet = getNavigationSet( nameOrId );

		function traverse( item, level ){


			_.each(item.children, function( child ){
				// get the full page element instead of just the nav item
				var pageInfo = _.findWhere( siteJson.pages, { id : child.navigable_id });

				// apply iterator function
				if( typeof iteratorFn === 'function' ){
					iteratorFn.call( window, pageInfo, level );
				}
				traverse( child, (level + 1) );
			});
		}

		traverse( navSet, 0 );
	}



	// HELPER FUNCTIONS
	
	function _isInt( x ){
	  var y = parseInt( x );

	  if( isNaN( y ) ){ 
	  	return false;
	  }

	  return ( x === y ) && ( x.toString() === y.toString() );
	}



	// RETURN

	_self = {
		load                       : load,
		getNavigationSet           : getNavigationSet,
		getAllPagesInNavigationSet : getAllPagesInNavigationSet,
		traverseNavSet             : traverseNavSet,
		json                       : function(){ return siteJson; },
	};

	return _self;
}());