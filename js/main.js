requirejs.config({
	baseUrl : 'js',
	paths : {
		backbone   : 'libs/backbone',
		jquery     : 'libs/jquery',
		json2      : 'libs/json2',
		marionette : 'libs/backbone.marionette',
		tpl        : 'libs/tpl',
		underscore : 'libs/lodash'
	},
	shim : {
		backbone : {
			deps    : ['jquery','underscore','json2'],
			exports : 'Backbone'
		},
		marionette : {
			deps    : ['backbone'],
			exports : 'Marionette'
		},
		underscore : {
			exports : '_'
		}
	}
});

require([
	'app',
	'router'
], function( App ){

	// Exposing App to window for inspection and debugging
	window.App = App;
	// Start the app!
	App.start();

});