define([
	'app'
], function( App ){
	
	App.module( 'Router', function( Router, App, Backbone, Marionette, $, _ ){

		// API
		
		var API = {
			
			// HOME
			showHome : function(){
				console.log('API:showHome');
				// App.Home.Controller.show();
			},

		};



		// EVENTS

		// home

		App.on('home:show', function(){
			Router.History.add('home:show');
			API.showHome();
		});

	});

	return App.Router;

});

